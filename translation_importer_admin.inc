<?php
/**
 * @file
 * provides admin form for translation_importer.
 */


/**
 * Form builder for the admin form for translation_importer.
 *
 * @ingroup forms
 */
function translation_importer_settings_form() {
  $form = array();
  $form['translation_importer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Translation importer Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['translation_importer']['translation_importer_enable_cache_clear'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable on cache clear'),
    '#default_value' => variable_get('translation_importer_enable_cache_clear', FALSE),
    '#description' => t('When enabled the import will be envoked on every cache clear'),
  );

  $form['translation_importer']['translation_importer_enable_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable on cron run'),
    '#default_value' => variable_get('translation_importer_enable_cron', FALSE),
    '#description' => t('When enabled the import will be envoked on every cron run. Only use this when cron isn\'t ran often.'),
  );

  $form['translation_importer']['file_translations_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Show debug information to all users'),
    '#default_value' => variable_get('file_translations_path', conf_path() . '/translations'),
    '#description' => t('Path where .po files can be found'),
  );

  return system_settings_form($form);
}
